package main

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type Book struct {
	ID     string  `json:"id"`
	ISBN   string  `json:"isbn"`
	Title  string  `json:"title"`
	Author *Author `json:"author"`
}

type Author struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

var books []Book

func index(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(books)
}

func show(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	params := mux.Vars(r)

	for _, item := range books {
		if item.ID == params["id"] {
			json.NewEncoder(w).Encode(item)
			return
		}
	}

	json.NewEncoder(w).Encode(&Book{})
}

func store(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var book Book

	_ = json.NewDecoder(r.Body).Decode(&book)

	book.ID = strconv.Itoa(rand.Intn(1000000))

	books = append(books, book)

	json.NewEncoder(w).Encode(book)
}

func update(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	params := mux.Vars(r)

	for index, item := range books {
		if item.ID == params["id"] {
			books = append(books[:index], books[index+1:]...)

			var book Book

			_ = json.NewDecoder(r.Body).Decode(&book)

			book.ID = strconv.Itoa(rand.Intn(1000000))

			books = append(books, book)

			json.NewEncoder(w).Encode(book)
		}
	}
}

func destroy(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	params := mux.Vars(r)

	for index, item := range books {
		if item.ID == params["id"] {
			books = append(books[:index], books[index+1:]...)
			break
		}
	}

	json.NewEncoder(w).Encode(books)
}

func main() {
	router := mux.NewRouter()

	books = append(books, Book{ID: "1", ISBN: "448743", Title: "Book One",
		Author: &Author{Firstname: "John", Lastname: "Doe"}})

	books = append(books, Book{ID: "2", ISBN: "968466", Title: "Book Two",
		Author: &Author{Firstname: "Jane", Lastname: "Smith"}})

	router.HandleFunc("/api/books", index).Methods("GET")
	router.HandleFunc("/api/books/{id}", show).Methods("GET")
	router.HandleFunc("/api/books", store).Methods("POST")
	router.HandleFunc("/api/books/{id}", update).Methods("PUT")
	router.HandleFunc("/api/books/{id}", destroy).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8000", router))
}
